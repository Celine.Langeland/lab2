package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {
    
    public final int maxNumberOfItems = 20;
    List <FridgeItem> fridge;
    
    public Fridge() {
        fridge = new ArrayList<>();
	} 
    
    @Override
    public int totalSize() {
        return maxNumberOfItems;
    }

    @Override
    public void emptyFridge() {
         int nItems = nItemsInFridge();
         if (nItems > 0) {
             fridge.clear();
        }  
    } 

    @Override
    public int nItemsInFridge() {
        int numberOfItems = fridge.size();
        return numberOfItems;
    }


    @Override
    public boolean placeIn(FridgeItem item) {
        int nItems = nItemsInFridge();
        int mItems = totalSize();
        if (nItems < mItems) {
            fridge.add(item);
            return true;
        }
        else {
            System.out.println("Fridge is full");
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
            fridge.remove(item);
        }
        else {
            throw new NoSuchElementException("Cannot take out element from empty fridge.");
        }
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List <FridgeItem> expired = new ArrayList<>();
        for (FridgeItem item : fridge) {
            if (item.hasExpired()) {
                expired.add(item);
            }
        }
        fridge.removeAll(expired);
        return expired;
    }
}
